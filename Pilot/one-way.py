import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as stats
from scipy.stats import shapiro
import statsmodels.api as sm
from scipy import stats
import seaborn as sns
import statsmodels.api as sm
from statsmodels.formula.api import ols

# Initialization of data files
study_list = [1, 2, 3, 4, 5]
file_type_list = ['HR', 'EDA']


# Lists for features, used to create data frame for anova test
HRmean = []
HRstd = []
study = []
EDAmean = []
EDAstd = []

for study_idx in range(0, len(study_list)):

    for file_type_idx in range(0, len(file_type_list)):

        if file_type_idx == 0:

            data_path = '/Users/rosalouisemarker/Desktop/Pilot/' + str(study_list[study_idx]) + '/' + file_type_list[
                file_type_idx] + '.csv'

            data = pd.read_csv(data_path, header=None)
            sig = data.iloc[62:]
            sig = sig.reset_index(drop=True)

            for seg in range(1):
                n = int(int(sig.size) // 60)
                m = int(int(int(sig.size) // 60) * 60)
                sig = sig.iloc[:m]

                # Segment data into n times 1 minute
                segments_HR = sig.iloc[:, 0].tolist()
                segments_HR = pd.DataFrame(np.array(segments_HR).reshape(n, 60).T)

                # Loop through HR data + participant ID and conditions
                if study_idx == 0:
                    # Create array with participant ID
                    study_ = np.ones(n, dtype=int)
                    study.extend(study_)


                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    HRstd.extend(std_HR)

                    # Check normality assumption by Shapiro-Wilkinson test
                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(HRmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR mean is normally distributed')
                    else:
                        print('HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(HRstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR std is normally distributed')
                    else:
                        print('HR std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_HR, plot=ax[0])
                    ax[0].set_title('Heart Rate Mean')
                    stats.probplot(std_HR, plot=ax[1])
                    fig.suptitle('QQ-plots of HR for pilot study 1', fontsize=16)
                    ax[1].set_title('Heart Rate Standard Deviation')
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of HR
                    time_mean = np.arange(0, len(mean_HR))
                    model_mean = sm.OLS(time_mean, mean_HR).fit()
                    # Make a linear model for the std of HR
                    time_std = np.arange(0, len(std_HR))
                    model_std = sm.OLS(time_std, std_HR).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of HR residuals for pilot study 1', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR mean is normally distributed')
                    else:
                        print('The residuals of HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR std is normally distributed')
                    else:
                        print('The residuals of HR std is not normally distributed')
                    print(' ')

                if study_idx == 1:
                    # Create array with participant ID
                    study_ = np.full(n, 2, dtype=int)
                    study.extend(study_)


                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    # std_HR = segments_HR.std(axis=1)
                    HRstd.extend(std_HR)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(HRmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR mean is normally distributed')
                    else:
                        print('HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(HRstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR std is normally distributed')
                    else:
                        print('HR std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_HR, plot=ax[0])
                    ax[0].set_title('Heart Rate Mean')
                    stats.probplot(std_HR, plot=ax[1])
                    fig.suptitle('QQ-plots of HR for pilot study 2', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of HR
                    time_mean = np.arange(0, len(mean_HR))
                    model_mean = sm.OLS(time_mean, mean_HR).fit()
                    # Make a linear model for the std of HR
                    time_std = np.arange(0, len(std_HR))
                    model_std = sm.OLS(time_std, std_HR).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of HR residuals for pilot study 2', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR mean is normally distributed')
                    else:
                        print('The residuals of HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR std is normally distributed')
                    else:
                        print('The residuals of HR std is not normally distributed')
                    print(' ')

                if study_idx == 2:
                    # Create array with participant ID
                    study_ = np.full(n, 3, dtype=int)
                    study.extend(study_)


                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    HRstd.extend(std_HR)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(HRmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR mean is normally distributed')
                    else:
                        print('HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(HRstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR std is normally distributed')
                    else:
                        print('HR std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_HR, plot=ax[0])
                    ax[0].set_title('Heart Rate Mean')
                    stats.probplot(std_HR, plot=ax[1])
                    ax[1].set_title('Heart Rate Standard Deviation')
                    fig.suptitle('QQ-plots of HR for pilot study 3', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of HR
                    time_mean = np.arange(0, len(mean_HR))
                    model_mean = sm.OLS(time_mean, mean_HR).fit()
                    # Make a linear model for the std of HR
                    time_std = np.arange(0, len(std_HR))
                    model_std = sm.OLS(time_std, std_HR).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of HR residuals for pilot study 3', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR mean is normally distributed')
                    else:
                        print('The residuals of HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR std is normally distributed')
                    else:
                        print('The residuals of HR std is not normally distributed')
                    print(' ')


                elif study_idx == 3:
                    # Create array with participant ID
                    study_ = np.full(n, 4, dtype=int)
                    study.extend(study_)


                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    HRstd.extend(std_HR)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(HRmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR mean is normally distributed')
                    else:
                        print('HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(HRstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR std is normally distributed')
                    else:
                        print('HR std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_HR, plot=ax[0])
                    ax[0].set_title('Heart Rate Mean')
                    stats.probplot(std_HR, plot=ax[1])
                    ax[1].set_title('Heart Rate Standard Deviation')
                    fig.suptitle('QQ-plots of HR for pilot study 4', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of HR
                    time_mean = np.arange(0, len(mean_HR))
                    model_mean = sm.OLS(time_mean, mean_HR).fit()
                    # Make a linear model for the std of HR
                    time_std = np.arange(0, len(std_HR))
                    model_std = sm.OLS(time_std, std_HR).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of HR residuals for pilot study 4', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR mean is normally distributed')
                    else:
                        print('The residuals of HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR std is normally distributed')
                    else:
                        print('The residuals of HR std is not normally distributed')
                    print(' ')

                if study_idx == 4:
                    # Create array with participant ID
                    study_ = np.full(n, 5, dtype=int)
                    study.extend(study_)

                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    HRstd.extend(std_HR)

                    print(' ')
                    print(' ')
                    print('Shapiro test for the baseline')
                    stat, p = shapiro(HRmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR mean is normally distributed')
                    else:
                        print('HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for the baseline')
                    stat, p = shapiro(HRstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('HR std is normally distributed')
                    else:
                        print('HR std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_HR, plot=ax[0])
                    ax[0].set_title('Heart Rate Mean')
                    stats.probplot(std_HR, plot=ax[1])
                    ax[1].set_title('Heart Rate Standard Deviation')
                    fig.suptitle('QQ-plots of HR for the baseline', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of HR
                    time_mean = np.arange(0, len(mean_HR))
                    model_mean = sm.OLS(time_mean, mean_HR).fit()
                    # Make a linear model for the std of HR
                    time_std = np.arange(0, len(std_HR))
                    model_std = sm.OLS(time_std, std_HR).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                                  ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of HR residuals for the baseline', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for the baseline')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR mean is normally distributed')
                    else:
                        print('The residuals of HR mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for the baseline')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of HR std is normally distributed')
                    else:
                        print('The residuals of HR std is not normally distributed')
                    print(' ')





        # Loop through EDA data
        elif file_type_idx == 1:

            data_path = '/Users/rosalouisemarker/Desktop/Pilot/' + str(study_list[study_idx]) + '/' + file_type_list[
                file_type_idx] + '.csv'

            data = pd.read_csv(data_path, header=None)
            sig = data.iloc[242:]
            sig = sig.reset_index(drop=True)

            for seg in range(1):
                n = int(int(sig.size) // 240)
                m = int(int(int(sig.size) // 240) * 240)
                sig = sig.iloc[:m]

                # Segment data into n times 1 minute
                segments_EDA = sig.iloc[:, 0].tolist()
                segments_EDA = pd.DataFrame(np.array(segments_EDA).reshape(n, 240).T)
                # print(segments_EDA)

                if study_idx == 0:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(EDAmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA mean is normally distributed')
                    else:
                        print('EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(EDAstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA std is normally distributed')
                    else:
                        print('EDA std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_EDA, plot=ax[0])
                    ax[0].set_title('EDA Mean')
                    stats.probplot(std_EDA, plot=ax[1])
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('QQ-plots of EDA for pilot study 1', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of EDA
                    time_mean = np.arange(0, len(mean_EDA))
                    model_mean = sm.OLS(time_mean, mean_EDA).fit()
                    # Make a linear model for the std of EDA
                    time_std = np.arange(0, len(std_EDA))
                    model_std = sm.OLS(time_std, std_EDA).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of EDA residuals for pilot study 1', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA mean is normally distributed')
                    else:
                        print('The residuals of EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA std is normally distributed')
                    else:
                        print('The residuals of EDA std is not normally distributed')
                    print(' ')

                if study_idx == 1:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(EDAmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA mean is normally distributed')
                    else:
                        print('EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(EDAstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA std is normally distributed')
                    else:
                        print('EDA std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_EDA, plot=ax[0])
                    ax[0].set_title('EDA Mean')
                    stats.probplot(std_EDA, plot=ax[1])
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('QQ-plots of EDA for pilot study 2', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of EDA
                    time_mean = np.arange(0, len(mean_EDA))
                    model_mean = sm.OLS(time_mean, mean_EDA).fit()
                    # Make a linear model for the std of EDA
                    time_std = np.arange(0, len(std_EDA))
                    model_std = sm.OLS(time_std, std_EDA).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of EDA residuals for pilot study 2', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA mean is normally distributed')
                    else:
                        print('The residuals of EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 2')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA std is normally distributed')
                    else:
                        print('The residuals of EDA std is not normally distributed')
                    print(' ')

                if study_idx == 2:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(EDAmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA mean is normally distributed')
                    else:
                        print('EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(EDAstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA std is normally distributed')
                    else:
                        print('EDA std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_EDA, plot=ax[0])
                    ax[0].set_title('EDA Mean')
                    stats.probplot(std_EDA, plot=ax[1])
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('QQ-plots of EDA for pilot study 3', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of EDA
                    time_mean = np.arange(0, len(mean_EDA))
                    model_mean = sm.OLS(time_mean, mean_EDA).fit()
                    # Make a linear model for the std of EDA
                    time_std = np.arange(0, len(std_EDA))
                    model_std = sm.OLS(time_std, std_EDA).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of EDA residuals for pilot study 3', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA mean is normally distributed')
                    else:
                        print('The residuals of EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 3')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA std is normally distributed')
                    else:
                        print('The residuals of EDA std is not normally distributed')
                    print(' ')


                elif study_idx == 3:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(EDAmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA mean is normally distributed')
                    else:
                        print('EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(EDAstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA std is normally distributed')
                    else:
                        print('EDA std is not normally distributed')
                    print(' ')

                    # Plot QQ-plots side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_EDA, plot=ax[0])
                    ax[0].set_title('EDA Mean')
                    stats.probplot(std_EDA, plot=ax[1])
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('QQ-plots of EDA for pilot study 4', fontsize=16)
                    plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of EDA
                    time_mean = np.arange(0, len(mean_EDA))
                    model_mean = sm.OLS(time_mean, mean_EDA).fit()
                    # Make a linear model for the std of EDA
                    time_std = np.arange(0, len(std_EDA))
                    model_std = sm.OLS(time_std, std_EDA).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                              ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of EDA residuals for pilot study 4', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA mean is normally distributed')
                    else:
                        print('The residuals of EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 4')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA std is normally distributed')
                    else:
                        print('The residuals of EDA std is not normally distributed')
                    print(' ')


                elif study_idx == 4:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)

                    print(' ')
                    print(' ')
                    print('Shapiro test for the baseline')
                    stat, p = shapiro(EDAmean)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA mean is normally distributed')
                    else:
                        print('EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for the baseline')
                    stat, p = shapiro(EDAstd)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('EDA std is normally distributed')
                    else:
                        print('EDA std is not normally distributed')
                    print(' ')

                    # Plot side by side
                    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
                    # Check normality assumption by Q-Q plot
                    stats.probplot(mean_EDA, plot=ax[0])
                    ax[0].set_title('EDA Mean')
                    stats.probplot(std_EDA, plot=ax[1])
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('QQ-plots of EDA for the baseline', fontsize=16)
                    # plt.show()

                    # Check if residuals are normally distributed
                    # Make a linear model for the mean of EDA
                    time_mean = np.arange(0, len(mean_EDA))
                    model_mean = sm.OLS(time_mean, mean_EDA).fit()
                    # Make a linear model for the std of EDA
                    time_std = np.arange(0, len(std_EDA))
                    model_std = sm.OLS(time_std, std_EDA).fit()
                    # Plot QQ-plots of residuals side by side
                    fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(10, 5))
                    sm.qqplot(model_mean.resid, stats.norm, fit=True, line='45',
                                  ax=ax1)
                    ax1.set_title('Residuals of mean')
                    sm.qqplot(model_std.resid, stats.norm, fit=True, line='45', ax=ax2)
                    ax2.set_title('Residuals of std')
                    fig.suptitle('QQ-plots of EDA residuals for the baseline', fontsize=16)
                    plt.show()
                    # Credit to https://slogix.in/how-to-check-whether-residuals-are-normally-distributed-or-not-using-python#Source-code

                    print(' ')
                    print(' ')
                    print('Shapiro test for the baseline')
                    stat, p = shapiro(model_mean.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA mean is normally distributed')
                    else:
                        print('The residuals of EDA mean is not normally distributed')
                    print(' ')

                    print('Shapiro test for pilot study 1')
                    stat, p = shapiro(model_std.resid)
                    print('stat = %.3f, p = %.3f\n' % (stat, p))
                    if p > 0.05:
                        print('The residuals of EDA std is normally distributed')
                    else:
                        print('The residuals of EDA std is not normally distributed')
                    print(' ')




# Remove the first observation in EDA to handle error
EDAmean.pop(0)
EDAstd.pop(0)

# Create empty data frame
column = ['A']
df = pd.DataFrame(columns=column)
df = df.drop(['A'], axis=1)
# Insert columns by appending lists of info
df['Pilot study'] = study
df['HR mean'] = HRmean
df['HR std'] = HRstd
df['EDA mean'] = EDAmean
df['EDA std'] = EDAstd
# Show data frame
print(df)
#print(df.to_latex())


# ANOVA tests
# HR mean
print(' ')
print('One-way ANOVA for HR mean')
model = ols('HRmean ~ C(study)', data=df).fit()
anova_HRmean = sm.stats.anova_lm(model, typ=2)
print(anova_HRmean)
#print(anova_HRmean.to_latex())

# HR std
print(' ')
print('One-way ANOVA for HR std')
model = ols('HRstd ~ C(study)', data=df).fit()
anova_HRstd = sm.stats.anova_lm(model, typ=2)
print(anova_HRstd)
#print(anova_HRstd.to_latex())

# EDA mean
print(' ')
print('One-way ANOVA for EDA mean')
model = ols('EDAmean ~ C(study)', data=df).fit()
anova_EDAmean = sm.stats.anova_lm(model, typ=2)
print(anova_EDAmean)
#print(anova_EDAmean.to_latex())

# EDA std
print(' ')
print('One-way ANOVA for EDA std')
model = ols('EDAstd ~ C(study)', data=df).fit()
anova_EDAstd = sm.stats.anova_lm(model, typ=2)
print(anova_EDAstd)
#print(anova_EDAstd.to_latex())



# Post hoc test, Bonferri correction
import statsmodels.stats.multicomp as mc

print(' ')
print('Post hoc test for HR mean')
comp = mc.MultiComparison(df['HR mean'], df['Pilot study'])
tbl, a1, a2 = comp.allpairtest(stats.ttest_ind, method= "bonf")
print(tbl)


print(' ')
print('Post hoc test for EDA mean')
comp = mc.MultiComparison(df['EDA mean'], df['Pilot study'])
tbl, a1, a2 = comp.allpairtest(stats.ttest_ind, method= "bonf")
print(tbl)
# Credit to https://www.pythonfordatascience.org/anova-python/



# Non parametric test, Kruskal-Wallis
from scipy import stats
stat, p  = stats.kruskal(study, HRmean)
print(' ')
print('Kruskal Wallis test for HR mean')
print('The p-value is ' f'{p}')
print('The stats is ' f'{stat}')

stat, p  = stats.kruskal(study, HRstd)
print(' ')
print('Kruskal Wallis test for HR std')
print('The p-value is ' f'{p}')
print('The stats is ' f'{stat}')


stat, p  = stats.kruskal(study, EDAmean)
print(' ')
print('Kruskal Wallis test for EDA mean')
print('The p-value is ' f'{p}')
print('The stats is ' f'{stat}')


stat, p  = stats.kruskal(study, EDAstd)
print(' ')
print('Kruskal Wallis test for EDA std')
print('The p-value is ' f'{p}')
print('The stats is ' f'{stat}')


# Post hoc test for non parametric test
import scikit_posthocs as sp
print(' ')
print('Post hoc test for non parametric HR mean')
table = sp.posthoc_dunn(df, val_col='HR mean', group_col='Pilot study')
print(table)
#print(table.to_latex())

print(' ')
print('Post hoc test for non parametric HR std')
table = sp.posthoc_dunn(df, val_col='HR std', group_col='Pilot study')
print(table)
#print(table.to_latex())

print(' ')
print('Post hoc test for non parametric EDA mean')
table = sp.posthoc_dunn(df, val_col='EDA mean', group_col='Pilot study')
print(table)
#print(table.to_latex())

print(' ')
print('Post hoc test for non parametric EDA std')
table = sp.posthoc_dunn(df, val_col='EDA std', group_col='Pilot study')
print(table)
#print(table.to_latex())


# Make boxplots of data
import seaborn as sns
box1 = sns.boxplot(x="Pilot study", y="HR mean", data=df)
plt.show()
box2 = sns.boxplot(x="Pilot study", y="HR std", data=df)
plt.show()
box3 = sns.boxplot(x="Pilot study", y="EDA mean", data=df)
plt.show()
box4 = sns.boxplot(x="Pilot study", y="EDA std", data=df)
plt.show()
