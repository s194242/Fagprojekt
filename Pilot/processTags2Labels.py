import pandas as pd
import numpy as np

pid_list = [1, 2, 3, 4, 5]

for pid_idx in pid_list:
    
    file_name_bvp = '/Users/rosalouisemarker/Desktop/Pilot/'+str(pid_idx)+'/BVP.csv'
    file_name = '/Users/rosalouisemarker/Desktop/Pilot/'+str(pid_idx)+'/tags.csv'
    tmp = pd.read_csv(file_name_bvp, header=None)
    start_time = tmp.iloc[0]
    recording_len_samples = tmp.shape[0]-2
    recording_len_sec = recording_len_samples/tmp.iloc[1]
    tag_list = pd.read_csv(file_name, header=None)
    tags_loc_in_secs = tag_list-start_time
    seconds_col = np.arange(5, np.float((np.ceil(recording_len_sec/5)+1)*5), 5)
    
    frustration_col = np.zeros(seconds_col.shape[0],)
    for idx in range(len(tags_loc_in_secs)):
        jdx = np.int(np.ceil(tags_loc_in_secs.iloc[idx]/5)-1)
        frustration_col[jdx,] = 1
        
    label_dict = {'seconds':[], 'frustration':[]}
    for idx in range(seconds_col.shape[0]):
        
        label_dict['seconds'].append(seconds_col[idx])
        label_dict['frustration'].append(frustration_col[idx])
    
    label_df = pd.DataFrame(label_dict)
    label_df.to_csv('/Users/rosalouisemarker/Desktop/Pilot/E4/'+str(pid_idx)+'/labels.csv',
                                  sep=',', float_format=np.float64,              
                                  index=False)
    print(label_df)