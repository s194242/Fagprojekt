import pandas as pd
import numpy as np

pid_list = [1, 2, 3, 4, 5]
device_list = ['A03891', 'A03891', 'A03891', 'A03891', 'A03891']
device_number = [1, 2, 3, 4, 5]

file_type_list = ['BVP', 'EDA', 'HR', 'TEMP', 'ACC']

for pid_idx in range(0, len(pid_list)):
    
    for file_type_idx in range(0, len(file_type_list)):
        
        if file_type_idx <= 3:
            
            data_path = '/Users/rosalouisemarker/Desktop/Pilot/'+str(pid_list[pid_idx])+'/'+file_type_list[file_type_idx]+'.csv'
            
            data = pd.read_csv(data_path, header=None)
            sig = data.iloc[2:]
            start_time = data.iloc[0]
            fs = data.iloc[1]
            
            tmp = np.arange(0, float(sig.shape[0]/fs), float(1/fs))
            time_stamp = float(start_time)*np.ones(sig.shape[0], )+tmp
            
            pid = pid_list[0]*np.ones(sig.shape[0], )
            d_s = [device_list[0]]*sig.shape[0]
            d_n = device_number[0]*np.ones(sig.shape[0], )
            e_t = float(start_time)*np.ones(sig.shape[0], )
            data_frame_dict = {'timestamp':time_stamp, 'pid': np.squeeze(pid), 'value': np.squeeze(sig), 
                               'device_serial': d_s, 'device_number': np.squeeze(d_n), 
                               'entry_time': np.squeeze(e_t)}
            data_frame = pd.DataFrame(data_frame_dict, dtype=np.float64)
            #data_frame = data_frame.reset_index()
            #data_frame = data_frame.drop(columns=['index'])
            data_frame.to_csv('/Users/rosalouisemarker/Desktop/Pilot/E4/'+str(pid_list[pid_idx])+'/E4_'+file_type_list[file_type_idx]+'.csv',
                              sep=',', float_format=np.float64, 
                              columns=['timestamp', 'pid', 'value', 'device_serial', 'device_number', 'entry_time'], 
                              index=False)
            
        elif file_type_idx == 4:
            
            data_path = '/Users/rosalouisemarker/Desktop/Pilot/'+str(pid_list[pid_idx])+'/'+file_type_list[file_type_idx]+'.csv'
            data = pd.read_csv(data_path, header=None)
            sig = data.iloc[2:]
            start_time = data.iloc[0]
            fs = data.iloc[1]
            
            tmp = np.arange(0, float(sig.shape[0]/fs[0]), float(1/fs[0]))
            time_stamp = float(start_time[0])*np.ones(sig.shape[0], )+tmp
            
            pid = pid_list[0]*np.ones(sig.shape[0], )
            d_s = [device_list[0]]*sig.shape[0]
            d_n = device_number[0]*np.ones(sig.shape[0], )
            e_t = float(start_time[0])*np.ones(sig.shape[0], )
            data_frame_dict = {'timestamp':time_stamp, 'pid': np.squeeze(pid), 'x': sig[0], 'y': sig[1], 'z': sig[2], 
                               'device_serial': d_s, 'device_number': np.squeeze(d_n), 'entry_time': np.squeeze(e_t)}
            data_frame = pd.DataFrame(data_frame_dict, dtype=np.float64)
            data_frame.to_csv('/Users/rosalouisemarker/Desktop/Pilot/E4/'+str(pid_list[pid_idx])+'/E4_'+file_type_list[file_type_idx]+'.csv',
                              sep=',', float_format=np.float64, 
                              columns=['timestamp', 'pid', 'x', 'y', 'z', 'device_serial', 'device_number', 'entry_time'], 
                              index=False)