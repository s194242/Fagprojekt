import os
import json
import logging
import argparse
import numpy as np
import pandas as pd

from collections import namedtuple
from datetime import datetime, timedelta
from logutils import init_logger


def aggregate_raw(paths, valid_pids):
    '''Aggregate raw data files by participant IDs and returns a dict.

    Args:
        paths (dict of str: paths to K-EmoCon dataset files): requires,
            e4_dir (str): path to a directory of raw Empatica E4 data files saved as CSV files
            h7_dir (str): path to a directory of raw Polar H7 data files saved as CSV files

        valid_pids (list of int): a list containing valid participant IDs

    Returns:
        pid_to_raw_df (dict of int: pandas DataFrame): maps participant IDs to DataFrames containing raw data.

    '''
    logger = logging.getLogger('default')
    e4_dir = paths['e4_dir']
    pid_to_raw_df = {}
    #valid_pids = [1, 2, 3, 4, 5]

    # store raw e4 data
    for pid in valid_pids:
        # get paths to e4 data files
        user_dir = os.path.join(e4_dir, str(pid))
        filepaths = [os.path.join(user_dir, f) for f in os.listdir(user_dir) if f != '.ipynb_checkpoints']

        # store e4 data files to dict as k = "{uid}/{filetype}" -> v = DataFrame
        for filepath in filepaths:
            try:
                filetype = filepath.split('/')[-1].split('_')[-1].split('.')[0].lower()
                filekey = f'{pid}/{filetype}'
                data = pd.read_csv(filepath)
                pid_to_raw_df[filekey] = data

            except Exception as err:
                logger.warning(f'Following exception occurred while processing {filekey}: {err}')

    return pid_to_raw_df


def get_baseline_and_debate(paths, valid_pids, filetypes, pid_to_raw_df):
    '''Split aggregated raw data files into baseline and debate dataframes.

    Args:
        paths (dict of str: paths to K-EmoCon dataset files): requires,
            e4_dir (str): path to a directory of raw Empatica E4 data files saved as CSV files
            h7_dir (str): path to a directory of raw Polar H7 data files saved as CSV files

        valid_pids (list of int)

        filetypes (list of str)

    Returns:
        pid_to_baseline_raw (dict of int: (dict of str: pandas Series))

        pid_to_debate_raw (dict of int: (dict of str: pandas Series))

    '''
    logger = logging.getLogger('default')
    pid_to_data = {pid:dict() for pid in valid_pids}


    # for each participant
    for pid in valid_pids:
        print('-' * 80)

        # for each filetype
        for filetype in filetypes:
            filekey = f'{pid}/{filetype}'
            try:
                data = pid_to_raw_df[filekey]
            except KeyError as err:
                logger.warning(f'Following exception occurred: {err}')
                continue
            
            pid_to_data[pid][filetype] = data.set_index('timestamp').value

    print('-' * 80)
    return pid_to_data


def baseline_to_json(paths, pid_to_baseline_raw):
    save_dir = paths['baseline_dir']
    # create a new directory if there isn't one already
    os.makedirs(save_dir, exist_ok=True)

    # for each participant
    for pid, baseline in pid_to_baseline_raw.items():

        # resample and interpolate ECG signals as they have duplicate entries while the intended frequency of ECG is 1Hz
        if 'ecg' in baseline.keys():
            ecg = baseline['ecg']
            ecg.index = pd.DatetimeIndex(ecg.index * 1e6)
            ecg = ecg.resample('1S').mean().interpolate(method='time')
            ecg.index = ecg.index.astype(np.int64) // 1e6
            baseline['ecg'] = ecg

        # convert sig values to list
        baseline = {sigtype: sig.values.tolist() for sigtype, sig in baseline.items() if sigtype in ['bvp', 'eda', 'temp', 'ecg']}

        # save baseline as json file
        savepath = os.path.join(save_dir, f'p{pid:02d}.json')
        with open(savepath, 'w') as f:
            json.dump(baseline, f, sort_keys=True, indent=4)

    return


def debate_segments_to_json(paths, valid_pids, filetypes, pid_to_debate_raw):
    save_dir = paths['segments_dir']

    # for each participant:
    print('-' * 100)
    print(f'{"pid"}\t{"debate_len":>10}\t{"sig_len":>10}\t{"s_len":>10}\t{"p_len":>10}\t{"e_len":>10}\t{"num_seg":>10}')
    for pid in valid_pids:
        os.makedirs(os.path.join(save_dir, str(pid)), exist_ok=True)
        signals = dict()

        # load debate data and self/partner/external annotation files
        debate = pid_to_debate_raw[pid]

        # find common timerange for signals
        sig_start, sig_end = 0, np.inf
        for sigtype in ['bvp', 'eda', 'temp']:
            sig_start = int(max(sig_start, debate[sigtype].index[0]))
            sig_end = int(min(sig_end, debate[sigtype].index[-1]))
        start_gap = 0
        end_gap = 0
        sig_len = sig_end - sig_start
        overlap = sig_len

        # second, cut singals w.r.t. common timerange
        for sigtype in ['bvp', 'eda', 'temp']:
            sig = debate[sigtype].loc[lambda x: (x.index >= sig_start) & (x.index < sig_end)]
            sig.index = sig.index.astype('float64')

            # also adjust start and end points of signals
            # this is necessary for consistency, as we want our signals within the overlapping duration
            diff = sig.index[-1] - sig.index[0] - overlap
            if diff > 0:
                start_diff = diff * start_gap / (start_gap + end_gap)
                end_diff = diff * end_gap / (start_gap + end_gap)
                sig = sig.loc[lambda x: (x.index >= sig.index[0] + start_diff) & (x.index < sig.index[-1] - end_diff)]


            signals[sigtype] = sig
            sig_len = min(sig_len, sig.index[-1] - sig.index[0])



        # find the greatest possible number of 5-second segments we can extract
        # the greatest possible number of segments is the maximum overlap across the length of debate data, self annotations, partner annotations, and external annotations divided by 5
        num_seg = int(sig_len // 5)
        
        # and their sides to match their lengths with signals (approximately equal to overlap)
        ## Read labels
        labels_cols = pd.read_csv('/Users/rosalouisemarker/Desktop/Pilot/E4/'+str(pid)+'/labels.csv')

      
        # get segments and save them as json files
        for i in range(num_seg):

            seg = dict()
            for sigtype in ['bvp', 'eda', 'temp']:
                sig = signals[sigtype]
                start = sig.index[0] + (i * 5)
                seg[sigtype] = sig.loc[lambda x: (x.index >= start) & (x.index < start + 5)].tolist()
            try:
                frustration = labels_cols['frustration'].iloc[i]
            except:
                frustration = 0
            s_v = str(1-np.int(frustration))
            s_a = str(np.int(frustration))
            p_a = '0'
            p_v = '0'
            e_a = '0'
            e_v = '0'
            seg_savepath = os.path.join(save_dir, str(pid), f'p{pid:02d}-{i:03d}-{s_a}{s_v}{p_a}{p_v}{e_a}{e_v}.json')
            with open(seg_savepath, 'w') as f:
                json.dump(seg, f, sort_keys=True, indent=4)
                
    print('-' * 100)
    return


if __name__ == "__main__":
    # initialize parser
    parser = argparse.ArgumentParser(description='Preprocess own dataset and save BVP, EDA, HST, and ECG signals as JSON files.')
    parser.add_argument('--root', '-r', type=str, default='/Users/rosalouisemarker/Desktop/Pilot/', required=False, help='a path to a root directory for the dataset')
    parser.add_argument('--timezone', '-t', type=str, default='UTC', help='a pytz timezone string for logger, default is UTC')
    args = parser.parse_args()

    # initialize default logger and constants
    logger = init_logger(tz=args.timezone)
    logger.info(f'Read/writing files to {args.root}...')
    PATHS = {
        'e4_dir': os.path.expanduser(os.path.join(args.root, 'E4')),
        'segments_dir': os.path.expanduser(os.path.join(args.root, 'segments')),
    }
    VALIDS = [1, 2, 3, 4, 5]
    FILETYPES = ['bvp', 'eda', 'hr', 'temp']

    # aggregate raw data
    logger.info('Preprocessing started, aggregating raw files...')
    pid_to_raw_df = aggregate_raw(PATHS, VALIDS)
    
    # get baseline and debate data
    logger.info('Getting baseline and debate data...')
    pid_to_raw = get_baseline_and_debate(PATHS, VALIDS, FILETYPES, pid_to_raw_df)


    # save 5s-segments
    logger.info(f'Saving debate segments as JSON files to {PATHS["segments_dir"]}...')
    debate_segments_to_json(PATHS, VALIDS, FILETYPES, pid_to_raw)
    logger.info('Preprocessing complete.')
