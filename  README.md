The files in the EmoCon folder processes the K-EmoCon dataset.
Run the scripts in the following order: logutils.py -> preprocess.py -> baseline_RosaI.py 
-> Feature importance.py, where Feature importance.py includes and plots the outputs of baseline_RosaI.py.
The files in the Nicole folder processes the data obtained during the fifth pilot study. 
Run the classification scripts in the following order: logutils.py -> otherData_preprocess.py 
-> processTags2Labels.py -> prepocess_wildData.py -> FeaturesLabels_WildData.py

Run the statistical script: anova.py and outliers.py is available, 
but the outputs are not included in the report (as described in the report). 
The files in the Pilot folder processes the data obtained during the first four
pilot studies and their associated baseline.

Run the classification scripts in the following order: logutils.py -> otherData_preprocess.py 
-> processTags2Labels.py -> prepocess_wildData.py -> Baseline_pilot.py
Run the statistical script: one-way.py