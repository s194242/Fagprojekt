import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as stats
from scipy.stats import shapiro
import statsmodels.api as sm
from scipy import stats
import seaborn as sns

# Initialization of data files
pid_list = [1, 2, 3, 4]
device_list = ['A03701', 'A03778', 'A03701', 'A02FC2']
device_number = [1, 2, 3, 4]
file_type_list = ['HR', 'EDA']


# Lists for features, used to create data frame
HRmean = []
HRstd = []
pid = []
con = []
EDAmean = []
EDAstd = []




for pid_idx in range(0, len(pid_list)):

    for file_type_idx in range(0, len(file_type_list)):

        if file_type_idx == 0:

            data_path = '/Users/rosalouisemarker/Desktop/Nicole/' + str(pid_list[pid_idx]) + '/' + file_type_list[
                file_type_idx] + '.csv'

            data = pd.read_csv(data_path, header=None)
            sig = data.iloc[62:]
            sig = sig.reset_index(drop=True)


            for seg in range(1):
                n = int(int(sig.size)//60)
                m = int(int(int(sig.size)//60)*60)
                sig = sig.iloc[:m]

                # Segment data into n times 1 minute
                segments_HR = sig.iloc[:, 0].tolist()
                segments_HR = pd.DataFrame(np.array(segments_HR).reshape(n, 60).T)
                #print(segments_HR)




                # Loop through HR data + participant ID and conditions
                if pid_idx == 0:
                    # Create array with participant ID
                    pid_ = np.zeros(n, dtype = int)
                    pid.extend(pid_)

                    # Create array with condition label
                    con_ = np.zeros(n, dtype = int)
                    con.extend(con_)

                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis = 0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis = 0))
                    HRstd.extend(std_HR)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_HR, 25)
                    Q2 = np.percentile(mean_HR, 50)
                    Q3 = np.percentile(mean_HR, 75)

                    print('Outlier detection in HR mean for participant 0 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRmean = []
                    for x in HRmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRmean.append(x)
                    print(' outlier in the subset is', outliers_HRmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_HR, 25)
                    Q2 = np.percentile(std_HR, 50)
                    Q3 = np.percentile(std_HR, 75)
                    print(' ')

                    print('Outlier detection in HR std for participant 0 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRstd = []
                    for x in HRstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRstd.append(x)
                    print(' outlier in the subset is', outliers_HRstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/







                if pid_idx == 1:
                    # Create array with participant ID
                    pid_ = np.ones(n, dtype = int)
                    pid.extend(pid_)

                    # Create array with condition label
                    con_ = np.zeros(n, dtype = int)
                    con.extend(con_)

                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis = 0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    #std_HR = segments_HR.std(axis=1)
                    HRstd.extend(std_HR)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_HR, 25)
                    Q2 = np.percentile(mean_HR, 50)
                    Q3 = np.percentile(mean_HR, 75)

                    print('Outlier detection in HR mean for participant 1 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRmean = []
                    for x in HRmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRmean.append(x)
                    print(' outlier in the subset is', outliers_HRmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_HR, 25)
                    Q2 = np.percentile(std_HR, 50)
                    Q3 = np.percentile(std_HR, 75)
                    print(' ')

                    print('Outlier detection in HR std for participant 1 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRstd = []
                    for x in HRstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRstd.append(x)
                    print(' outlier in the subset is', outliers_HRstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/




                if pid_idx == 2:
                    # Create array with participant ID
                    pid_ = np.zeros(n, dtype=int)
                    pid.extend(pid_)

                    # Create array with condition label
                    con_ = np.ones(n, dtype=int)
                    con.extend(con_)

                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    HRstd.extend(std_HR)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_HR, 25)
                    Q2 = np.percentile(mean_HR, 50)
                    Q3 = np.percentile(mean_HR, 75)

                    print('Outlier detection in HR mean for participant 0 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRmean = []
                    for x in HRmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRmean.append(x)
                    print(' outlier in the subset is', outliers_HRmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_HR, 25)
                    Q2 = np.percentile(std_HR, 50)
                    Q3 = np.percentile(std_HR, 75)
                    print(' ')

                    print('Outlier detection in HR std for participant 0 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRstd = []
                    for x in HRstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRstd.append(x)
                    print(' outlier in the subset is', outliers_HRstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/




                elif pid_idx == 3:
                    # Create array with participant ID
                    pid_ = np.ones(n, dtype=int)
                    pid.extend(pid_)

                    # Create array with condition labeln_HR)
                    con_ = np.ones(n, dtype=int)
                    con.extend(con_)

                    # Compute mean values of segments
                    mean_HR = np.array(np.mean(segments_HR, axis=0))
                    HRmean.extend(mean_HR)

                    # Compute standard deviation values of segments
                    std_HR = np.array(np.std(segments_HR, axis=0))
                    HRstd.extend(std_HR)



                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_HR, 25)
                    Q2 = np.percentile(mean_HR, 50)
                    Q3 = np.percentile(mean_HR, 75)

                    print('Outlier detection in HR mean for participant 1 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRmean = []
                    for x in HRmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRmean.append(x)
                    print(' outlier in the subset is', outliers_HRmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_HR, 25)
                    Q2 = np.percentile(std_HR, 50)
                    Q3 = np.percentile(std_HR, 75)
                    print(' ')

                    print('Outlier detection in HR std for participant 1 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_HRstd = []
                    for x in HRstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_HRstd.append(x)
                    print(' outlier in the subset is', outliers_HRstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/







        # Loop through EDA data
        elif file_type_idx == 1:

            data_path = '/Users/rosalouisemarker/Desktop/Nicole/' + str(pid_list[pid_idx]) + '/' + file_type_list[
                file_type_idx] + '.csv'

            data = pd.read_csv(data_path, header=None)
            sig = data.iloc[242:]
            sig = sig.reset_index(drop=True)

            for seg in range(1):
                n = int(int(sig.size)//240)
                m = int(int(int(sig.size)//240)*240)
                sig = sig.iloc[:m]

                # Segment data into n times 1 minute
                segments_EDA = sig.iloc[:, 0].tolist()
                segments_EDA = pd.DataFrame(np.array(segments_EDA).reshape(n, 240).T)
                #print(segments_EDA)


                if pid_idx == 0:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_EDA, 25)
                    Q2 = np.percentile(mean_EDA, 50)
                    Q3 = np.percentile(mean_EDA, 75)

                    print('Outlier detection in EDA mean for participant 0 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAmean = []
                    for x in EDAmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAmean.append(x)
                    print(' outlier in the subset is', outliers_EDAmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_EDA, 25)
                    Q2 = np.percentile(std_EDA, 50)
                    Q3 = np.percentile(std_EDA, 75)
                    print(' ')

                    print('Outlier detection in EDA std for participant 0 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAstd = []
                    for x in EDAstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAstd.append(x)
                    print(' outlier in the subset is', outliers_EDAstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/

                    # Plot boxplots side by side
                    fig, ax = plt.subplots(1, 2, figsize=(8, 6))
                    # Boxplots showing outliers
                    ax[0].boxplot(mean_EDA, )
                    ax[0].set_title('EDA Mean')
                    ax[1].boxplot(std_EDA)
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('Boxplots of EDA for participant 0 in condition 0', fontsize=16)





                if pid_idx == 1:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_EDA, 25)
                    Q2 = np.percentile(mean_EDA, 50)
                    Q3 = np.percentile(mean_EDA, 75)

                    print('Outlier detection in EDA mean for participant 1 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAmean = []
                    for x in EDAmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAmean.append(x)
                    print(' outlier in the subset is', outliers_EDAmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_EDA, 25)
                    Q2 = np.percentile(std_EDA, 50)
                    Q3 = np.percentile(std_EDA, 75)
                    print(' ')

                    print('Outlier detection in EDA std for participant 1 in condition 0')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAstd = []
                    for x in EDAstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAstd.append(x)
                    print(' outlier in the subset is', outliers_EDAstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/

                    # Plot boxplots side by side
                    fig, ax = plt.subplots(1, 2, figsize=(8, 6))
                    # Boxplots showing outliers
                    ax[0].boxplot(mean_EDA, )
                    ax[0].set_title('EDA Mean')
                    ax[1].boxplot(std_EDA)
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('Boxplots of EDA for participant 1 in condition 0', fontsize=16)






                if pid_idx == 2:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_EDA, 25)
                    Q2 = np.percentile(mean_EDA, 50)
                    Q3 = np.percentile(mean_EDA, 75)

                    print('Outlier detection in EDA mean for participant 0 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAmean = []
                    for x in EDAmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAmean.append(x)
                    print(' outlier in the subset is', outliers_EDAmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_EDA, 25)
                    Q2 = np.percentile(std_EDA, 50)
                    Q3 = np.percentile(std_EDA, 75)
                    print(' ')

                    print('Outlier detection in EDA std for participant 0 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAstd = []
                    for x in EDAstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAstd.append(x)
                    print(' outlier in the subset is', outliers_EDAstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/

                    # Plot boxplots side by side
                    fig, ax = plt.subplots(1, 2, figsize=(8, 6))
                    # Boxplots showing outliers
                    ax[0].boxplot(mean_EDA, )
                    ax[0].set_title('EDA Mean')
                    ax[1].boxplot(std_EDA)
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('Boxplots of EDA for participant 0 in condition 1', fontsize=16)






                elif pid_idx == 3:
                    # Compute mean values of segments
                    mean_EDA = np.array(np.mean(segments_EDA, axis=0))
                    EDAmean.extend(mean_EDA)

                    # Compute standard deviation values of segments
                    std_EDA = np.array(np.std(segments_EDA, axis=0))
                    EDAstd.extend(std_EDA)


                    # Check for outliers
                    # Detect outliers in mean values
                    Q1 = np.percentile(mean_EDA, 25)
                    Q2 = np.percentile(mean_EDA, 50)
                    Q3 = np.percentile(mean_EDA, 75)

                    print('Outlier detection in EDA mean for participant 1 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)


                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAmean = []
                    for x in EDAmean:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAmean.append(x)
                    print(' outlier in the subset is', outliers_EDAmean)

                    # Detect outliers in std values
                    Q1 = np.percentile(std_EDA, 25)
                    Q2 = np.percentile(std_EDA, 50)
                    Q3 = np.percentile(std_EDA, 75)
                    print(' ')


                    print('Outlier detection in EDA std for participant 1 in condition 1')
                    print(' ')
                    print('Q1 25 percentile of the given data is, ', Q1)
                    print('Q2 50 percentile of the given data is, ', Q2)
                    print('Q3 75 percentile of the given data is, ', Q3)

                    IQR = Q3 - Q1
                    print('Interquartile range is', IQR)

                    low_lim = Q1 - 1.5 * IQR
                    up_lim = Q3 + 1.5 * IQR
                    print('low_limit is', low_lim)
                    print('up_limit is', up_lim)

                    outliers_EDAstd = []
                    for x in EDAstd:
                        if ((x > up_lim) or (x < low_lim)):
                            outliers_EDAstd.append(x)
                    print(' outlier in the subset is', outliers_EDAstd)
                    # Credit to https://www.geeksforgeeks.org/interquartile-range-to-detect-outliers-in-data/

                    # Plot boxplots side by side
                    fig, ax = plt.subplots(1, 2, figsize=(8, 6))
                    # Boxplots showing outliers
                    ax[0].boxplot(mean_EDA, )
                    ax[0].set_title('EDA Mean')
                    ax[1].boxplot(std_EDA)
                    ax[1].set_title('EDA Standard Deviation')
                    fig.suptitle('Boxplots of EDA for participant 1 in condition 1', fontsize=16)
                    ax[0].set_ylim([0,9])
                    ax[1].set_ylim([0, 6])
                    plt.show()



# Create empty data frame
column = ['A']
df = pd.DataFrame(columns=column)
df = df.drop(['A'], axis=1)
# Insert columns by appending lists of info
df['Participant'] = pid
df['Condition'] = con
df['HR mean'] = HRmean
df['HR std'] = HRstd
df['EDA mean'] = EDAmean
df['EDA std'] = EDAstd
# Show data frame
print(df)


